function Slider (options) {

    let self = {};

    self.element = options.sliderId;

    self.init = () => {

        self.index = options.activeIndex;
        self.width = options.slideWidth;

        self.wrapper = self.element.getElementsByClassName('slider-wrapper')[0];
        self.slides = self.element.getElementsByClassName('slide-item');
        self.slideLeft = self.element.getElementsByClassName('slider-control-prev')[0];
        self.slideRight = self.element.getElementsByClassName('slider-control-next')[0];
        self.indicators = self.element.getElementsByClassName('slider-indicator');
        self.indicatorBlock = self.element.getElementsByClassName('slider-indicators')[0];
        self.indicators[self.index].classList.add('active');

        self.wrapperWidth = (self.width * self.slides.length)+'px';
        self.wrapper.style.width = self.wrapperWidth;

        for(let i = 0; i < self.slides.length; i++){
            self.slides[i].style.width = self.width + 'px';
        }
        self.wrapper.style.transform = 'translate3d('+ (-self.index*self.width) + 'px, 0, 0)';
    };
    self.init();

    self.changeSize = () => {
        window.addEventListener("resize", function() {
            self.width = innerWidth;
            self.wrapperWidth = (self.width * self.slides.length)+'px';
            self.wrapper.style.width = self.wrapperWidth;
            for(let i = 0; i < self.slides.length; i++){
                self.slides[i].style.width = self.width + 'px';
            }
            self.wrapper.style.transform = 'translate3d('+ (-self.index*self.width) + 'px, 0, 0)';
        }, false);
    };
    self.changeSize();

    self.moveSlide = (index) => {
        self.wrapper.style.transition = 'transform .5s ease';
        self.wrapper.style.transform = 'translate3d('+ (-index*self.width) + 'px, 0, 0)';
        let indicatorIndex;
        indicatorIndex = index === 0 ? self.indicators.length-2 : index === self.indicators.length-1 ? 1 : index;
        self.indicators[indicatorIndex].classList.add('active');
    };

    self.next = () => {
        self.index >= self.slides.length-1 ? false : self.index++;
        for(let i = 0; i < self.indicators.length; i++){
            self.indicators[i].classList.remove('active');
        }
        self.moveSlide(self.index);
        self.changeSlide();
    };

    self.prev = () => {
        self.index <= 0 ? false : self.index--;
        for(let i = 0; i < self.indicators.length; i++){
            self.indicators[i].classList.remove('active');
        }
        self.moveSlide(self.index);
        self.changeSlide();
    };

    self.changeSlide = () => {
        setTimeout(() => {
            self.wrapper.style.transition = 'none';
            self.slides[self.index].id === 'first-clone' ? self.index = 1 : self.index;
            self.slides[self.index].id === 'last-clone' ? self.index = self.slides.length-2 : self.index;
            self.wrapper.style.transform = 'translate3d('+ (-self.index*self.width) + 'px, 0, 0)';
        },600);
    };

    self.moveIndicator = (node) => {
        for(let i = 0; i < self.indicators.length; i++){
          self.indicators[i].classList.remove('active');
        }
        self.index = node.id;
        self.moveSlide(self.index);
    };

   self.autoSliding = () =>{
       let t;
       self.element.addEventListener("mouseleave", function(event) {
           t = setInterval(() => {
               self.next()
           },3000);
       });
       self.element.addEventListener("mouseenter", function(event) {
           window.clearTimeout(t);
       });
   };
    self.autoSliding();

    self.indicatorBlock.onclick = function(event) {
        let target = event.target;
        if (target.tagName != 'LI') return;
        self.moveIndicator(target);
    };
    self.slideLeft.addEventListener('click', () => { self.prev()});
    self.slideRight.addEventListener('click',() => { self.next()});

    return self;
}
